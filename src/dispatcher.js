define([
    'underscore',
    'backbone'
], function(_, Backbone) {
    'use strict';
    var dispatcher = _.clone(Backbone.Events);
    return dispatcher;
});
