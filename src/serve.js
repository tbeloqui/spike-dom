var express = require('express');
var app = express();
var arduino = require('duino');
var http = require('http');
var logger = require('morgan');
var bodyParser = require('body-parser');
var router = express.Router();
var BoardState = {
    UNINITIALIZED: 'uninitialized',
    CONNECTED: 'connected',
    ERROR: 'error'
};
var LEDMap = {
    'HabitacionPrincipal': '13',
    'SegundaHabitacion': '12',
    'TercerHabitacion': '11',
    'CuartaHabitacion': '10',
    'Comedor': '09',
    'BañoPrincipal': '08'
};
var board = new arduino.Board({
    debug: true
});

board.on('ready', function() {

    for (var i in LEDMap) {
        board.pinMode(LEDMap[i], 'out');
    }

});
router.post('/arduino_state', function(req, res, next) {
    if (!board.serial.paused) {
        res.send('connected');
    } else {
        res.send('disconnected');
    }
});

router.post('/set_light', function(req, res) {
    var val = board.LOW;
    if (req.body.enable === 'true') {
        val = board.HIGH;
    }

    var id = LEDMap[req.body.id];
    if (id) {
        board.digitalWrite(id, val);
    } else if (req.body.id == 'all') {
        for (var i in LEDMap) {
            id = LEDMap[i];
            board.digitalWrite(id, val);
        }
    }

    return res.end();

});
app.use(logger('dev'));
app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use('/', router);

var server = http.createServer(app);
server.listen(8080, function() {
    console.log('Express server listening on port ' + 8080);
});
