require([
    'backbone',
    'router',
    'views/housemap',
    'mock/dataProvider',
    'controllers/arduino'
], function(Backbone, Router, HouseMapView, dataProvider, arduinoController) {
    'use strict';
    new Router();
    Backbone.history.start();
    new HouseMapView(dataProvider.lightBulbs);
    arduinoController.initialize();
});
