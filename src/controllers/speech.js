define([
    'dispatcher',
    'spike'
], function(dispatcher, Spike) {
    'use strict';

    var speech = new Spike.SpeechModule(Spike.SpeechModule.CMUSPHINX);
    // var speech = new Spike.SpeechModule(Spike.SpeechModule.WEBKIT);

    speech.on('ready', function() {
        dispatcher.trigger('speech:ready');
    });

    return speech;
});
