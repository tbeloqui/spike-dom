define([
    'jquery',
    'dispatcher'
], function($, dispatcher) {
    'use strict';

    var controller = {
        initialize: function() {

            setInterval(function() {
                $.post("/arduino_state", function(data) {

                    if (data === 'connected') {
                        dispatcher.trigger('arduino:connected', true);
                    } else {
                        dispatcher.trigger('arduino:connected', false);
                    }
                }).fail(function() {
                    dispatcher.trigger('arduino:connected', false);
                });
            }, 1 * 1000);

        },
        set: function(id, enable) {

            $.post('/set_light', {
                id: id,
                enable: enable
            });
        }
    };

    return controller;
});
