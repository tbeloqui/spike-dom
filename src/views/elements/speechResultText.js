define([
    'underscore',
    'controllers/speech'
], function(_, speech) {

    function SpeechResultText(svg) {
        this.text = svg.text(350, 400, '').attr({
            fontSize: '12px'
        });

        var that = this;
        speech.on('result', function(matches) {

            var str = '- ',
                splits = matches.split(" ");
            _.each(splits, function(match) {
                str += match + ' - ';
            });

            that.render(str);
        });
    }

    SpeechResultText.prototype = {
        constructor: SpeechResultText,

        render: function(text) {

            this.text.attr({
                text: text,
                fill: 'white'
            });

        },

    };

    return SpeechResultText;
});
