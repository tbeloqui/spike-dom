/*global define*/
define([
    'underscore',
    'snapsvg',
    'dispatcher'
], function(_, Snap, dispatcher) {
    'use strict';


    function Arduino() {
        this.circle = null;
        this.container = null;

        var self = this;
        dispatcher.on('arduino:connected', function(connected) {

            var color = connected ? 'lime' : 'red';
            self.circle.attr({
                stroke: color,
                fill: color
            });

        });
    }

    Arduino.prototype = {

        constructor: Arduino,

        render: function(c) {
            var self = this;
            Snap.load('assets/svg/elements/arduino.svg', function(f) {

                var g = f.select("#breadboardbreadboard");
                var m = new Snap.Matrix();
                m.scale(0.4);
                m.translate(900, 50);

                g.attr({
                    transform: m
                });

                self.circle = c.circle(450, 30, 5).attr({
                    stroke: 'lime',
                    fill: 'lime'
                });

                c.append(g);

            });
        }
    };

    return Arduino;
});
