/*global define*/
define([
    'underscore',
    'snapsvg',
    'helpers/svgDragMove',
    'controllers/arduino'
], function(_, Snap, svgDragMove, arduino) {
    'use strict';

    function animateLightOn(bulb, beams) {
        var beamRects = beams.selectAll('rect');
        var bulbPaths = bulb.selectAll('path');
        bulb.animate({
            transform: 's.8,.8 t0,8'
        }, 300, mina.easeout, function() {
            bulbPaths.forEach(function(el, i) {
                el.attr({
                    fill: '#f5d76e',
                    opacity: 1
                });
            });
        });

        beams.animate({
            transform: 's1.2,1.2 t0,3',
            opacity: 1
        }, 200, mina.bounce, function() {
            beamRects.forEach(function(el, i) {
                el.stop().animate({
                    fill: '#f5d76e',
                    opacity: 1
                }, 200, mina.easeout);
            });
        });
    };

    function animateLightOff(bulb, beams) {
        var beamRects = beams.selectAll('rect');
        var bulbPaths = bulb.selectAll('path');

        beamRects.forEach(function(el, i) {
            el.stop().animate({
                fill: 'white',
                opacity: 0
            }, 300, mina.easeout);
        });

        bulb.stop().animate({
            transform: 's1,1 t0,0',
            opacity: 1
        }, 300, mina.easeout, function() {
            bulbPaths.forEach(function(el, i) {
                el.stop().animate({
                    fill: 'white'
                }, 200, mina.easeout);
            });
        });
    }

    function LightBulb() {
        this.isLightOn = false;
        this.bulb = null;
        this.beams = null;
    }

    LightBulb.prototype = {
        constructor: LightBulb,

        render: function(c, options) {
            var self = this;
            Snap.load('assets/svg/elements/light-bulb.svg', function(fragment) {
                self.bulb = fragment.select('#bulb');
                c.append(self.bulb);

                self.beams = fragment.select('#beams');
                self.beams.attr('opacity', 0);
                c.append(self.beams);

                var g = c.group(self.bulb, self.beams);
                var m = new Snap.Matrix();
                m.scale(0.4);
                m.translate(options.x, options.y);
                g.attr({
                    stroke: "#000",
                    strokeWidth: 2,
                    transform: m
                });

                svgDragMove.apply(g);
            });
        },

        turnOn: function() {
            if (!this.isLightOn) {
                animateLightOn(this.bulb, this.beams);
                this.isLightOn = true;
            }
        },

        turnOff: function() {
            if (this.isLightOn) {
                animateLightOff(this.bulb, this.beams);
                this.isLightOn = false;
            }
        },

    };

    return LightBulb;
});
