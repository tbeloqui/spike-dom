define([
    'jquery',
    'snapsvg',
    'controllers/speech'
], function($, Snap, speech) {
    'use strict';

    function toggleRecording(mic) {

        if (mic.active) {
            speech.stop();
        } else {
            speech.start();
        }
    }

    function turnWhite(mic, el) {
        el.attr({
            fill: 'white',
            opacity: '0.75'
        });
        mic.active = false;
        //
        // el.animate({
        //     fill: 'white',
        //     opacity: '0.75'
        // }, 100, mina.easeout, function() {
        //     mic.active = false;
        // });
    }

    function turnRed(mic, el) {
        el.attr({
            fill: '#FF0000',
            opacity: '0.75'
        });
        mic.active = true;
        // el.animate({
        //     fill: '#FF0000',
        //     opacity: '0.75'
        // }, 100, mina.easeout, function() {
        //     mic.active = true;
        // });
    }

    function wobble(mic, t, el) {

        if (t <= 0) {
            el.stop().animate({
                transform: 'r0 48,96'
            }, 100, mina.easeout, function() {
                turnRed(mic, el)
            });
            return;
        }
        var half = Math.round(t / 2);

        el.stop().animate({
            transform: 'r' + half + ' 48,96'
        }, 100 * half, mina.easeout, function() {
            el.stop().animate({
                transform: 'r-' + half + ' 48,96'
            }, 100 * half, mina.easeout, function() {
                --t;
                wobble(mic, t, el);
            });
        });

    }

    function Microphone() {
        this.active = false;
        this.ready = false;
        var that = this;

        speech.on('started', function() {
            that.showRecording();
        });

        speech.on('stopped', function() {
            that.hideRecording();
        });

        speech.on('ready', function() {
            that.ready = true;
            that.rect.addClass('hover_group');
            that.show();
        });

    }

    Microphone.prototype = {
        constructor: Microphone,

        render: function(s) {
            var that = this;
            Snap.load("assets/svg/elements/mic.svg", function(f) {

                that.group = f.select('g');
                s.append(that.group);

                var m = new Snap.Matrix();
                m.scale(0.55);
                m.translate(0, 35);
                that.group.attr({
                    fill: 'white',
                    opacity: 0.1,
                    transform: m
                });

                var bb = that.group.getBBox();
                that.rect = s.rect(bb.x, bb.y, bb.width, bb.height);

                that.rect.attr({
                    opacity: 0
                });

                that.mic = that.group.select('#mic');

                that.rect.click(function() {
                    if (that.ready) {
                        toggleRecording(that);
                    }
                });


            });
        },

        show: function() {
            this.group.animate({
                    opacity: '0.75',
                },
                1000,
                mina.elastic
            );
        },

        showRecording: function() {
            if (!this.active) {
                turnRed(this, this.mic);
            }

        },

        hideRecording: function() {
            if (this.active) {
                turnWhite(this, this.mic);
            }

        }

    };

    return Microphone;
});
