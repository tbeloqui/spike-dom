/*global define*/
define([
    'jquery',
    'underscore',
    'backbone',
    'snapsvg',
    'dispatcher',
    'views/elements/lightBulb',
    'views/elements/microphone',
    'views/elements/speechResultText',
    'views/elements/arduino',
    'text!templates/houseMapView.html'
], function($, _, Backbone, Snap, dispatcher, LightBulb, Microphone, SpeechResultText, Arduino, houseMapViewTemplate) {
    'use strict';

    var _view;

    _view = Backbone.View.extend({

        el: '#housemap',
        template: _.template(houseMapViewTemplate),

        initialize: function(lightBulbs) {
            this.model = new Backbone.Model();
            this.render(lightBulbs);
            this.text = null;
        },

        render: function(lightBulbs) {
            this.$el.html(this.template(this.model.toJSON()));

            var s = Snap('#svgMain');
            var that = this;

            Snap.load("assets/svg/maps/second_floor_509.svg", function(f) {

                s.append(f);

                that.text = new SpeechResultText(s);
                _.each(lightBulbs, function(options) {
                    var lightBulb = new LightBulb();
                    lightBulb.render(s, options);

                    dispatcher.on('bulb:turnOn:' + options.id, function() {
                        lightBulb.turnOn();
                    });

                    dispatcher.on('bulb:turnOff:' + options.id, function() {
                        lightBulb.turnOff();
                    });

                });

                var mic = new Microphone();
                mic.render(s);


                var arduino = new Arduino();
                arduino.render(s);

            });
        },

    });

    return _view;
});
