require.config({
    shim: {
        jquery: {
            exports: '$'
        },
        bootstrap: {
            "deps": ['jquery']
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        backboneLocalstorage: {
            deps: ['backbone'],
            exports: 'Store'
        },
        snap: {
            exports: 'Snap'
        },
        spike: {
            exports: 'Spike'
        }
    },
    paths: {
        jquery: 'lib/jquery',
        bootstrap: 'lib/bootstrap',
        underscore: 'lib/underscore',
        backbone: 'lib/backbone',
        backboneLocalstorage: 'lib/backbone.localStorage',
        text: 'lib/text',
        spike: 'lib/spike',
        snapsvg: 'lib/snap.svg'
    }
});
require(['main']);
