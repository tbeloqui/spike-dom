/*global define*/
define([
  'backbone'
], function (Backbone) {
  'use strict';

  var Router = Backbone.Router.extend({
    routes: {
      '': 'index'
    },

    index: function () {
    }
  });

  return Router;
});
