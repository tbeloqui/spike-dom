define([
    'underscore',
    'dispatcher',
    'helpers/sphinxHelper',
    'controllers/speech',
    'controllers/arduino'
], function(_, dispatcher, sphinxHelper, speech, arduino) {
    'use strict';

    var provider;

    function setupCMUSphinx() {

        speech.addWords([
            ['JUGAR', 'J U G A R'],
            ['EN', 'E N'],
            ['ENCENDER', 'E N S E N D E R'],
            ['APAGAR', 'A P A G A R'],
            ['BAÑO', 'B A N I O'],
            ['COMEDOR', 'K O M E D O R'],
            ['PRINCIPAL', 'P R I N S I P A L'],
            ['HABITACION', 'A B I T A S I O N'],
            ['SEGUNDA', ' S E G U N D A'],
            ['TERCER', 'T E R S E R'],
            ['CUARTA', 'K U A R T A'],
            ['COMPLETO', 'K O M P L E T O']
        ]);

        /* initial: 0, final: 3 */
        var transitions = _.union(
            sphinxHelper.buildTransitions(0, 1, ['JUGAR']),
            sphinxHelper.buildTransitions(1, 2, ['EN']),
            sphinxHelper.buildTransitions(2, 5, ['COMEDOR']),

            sphinxHelper.buildTransitions(0, 3, ['ENCENDER', 'APAGAR']),
            sphinxHelper.buildTransitions(3, 5, ['COMPLETO', 'COMEDOR']),
            sphinxHelper.buildTransitions(3, 4, ['SEGUNDA', 'TERCER', 'CUARTA', 'HABITACION', 'BAÑO']),
            sphinxHelper.buildTransitions(4, 5, ['HABITACION', 'PRINCIPAL'])
        ); 

        var grammar = {
            numStates: 6,
            start: 0,
            end: 5,
            transitions: transitions
        };

        speech.addGrammar(grammar);
    }

    function setupSpeech() {

        _.each(provider.lightBulbs, function(bulb) {

            if (!_.isEmpty(bulb.id)) {
                speech.add(bulb.turnOnPhrase, function(matches) {
                    dispatcher.trigger('bulb:turnOn:' + bulb.id);

                    speech.stop();
                    arduino.set(bulb.id, true);
                });

                speech.add(bulb.turnOffPhrase, function(matches) {
                    dispatcher.trigger('bulb:turnOff:' + bulb.id);

                    speech.stop();
                    arduino.set(bulb.id, false);
                });
            }

        });

        speech.add('encender completo', function(matches) {
            speech.stop();
            _.each(provider.lightBulbs, function(bulb) {
                dispatcher.trigger('bulb:turnOn:' + bulb.id);
            });
            arduino.set('all', true);
        });

        speech.add('apagar completo', function(matches) {
            speech.stop();
            _.each(provider.lightBulbs, function(bulb) {
                dispatcher.trigger('bulb:turnOff:' + bulb.id);
            });
            arduino.set('all', false);
        });

        speech.add('jugar en comedor', function(matches){
            speech.stop();
            window.location.href = '/game';
        });

    }

    function setup() {
        setupCMUSphinx();
        setupSpeech();
    }

    var lightBulbs = [{
        id: 'HabitacionPrincipal',
        turnOnPhrase: 'encender habitacion principal',
        turnOffPhrase: 'apagar habitacion principal',
        x: 128 * 2 + (128 / 2),
        y: 190
    }, {
        id: 'BañoPrincipal',
        turnOnPhrase: 'encender baño principal',
        turnOffPhrase: 'apagar baño principal',
        x: 283 * 2 + (283 / 2),
        y: 126
    }, {
        id: 'Comedor',
        turnOnPhrase: 'encender comedor',
        turnOffPhrase: 'apagar comedor',
        x: 137 * 2 + (137 / 2),
        y: 400
    }, {
        id: 'SegundaHabitacion',
        turnOnPhrase: 'encender segunda habitacion',
        turnOffPhrase: 'apagar segunda habitacion',
        x: 239 * 2 + (239 / 2),
        y: 485
    }, {
        id: 'TercerHabitacion',
        turnOnPhrase: 'encender tercer habitacion',
        turnOffPhrase: 'apagar tercer habitacion',
        x: 49 * 2 + (49 / 2),
        y: 860
    }, {
        id: 'CuartaHabitacion',
        turnOnPhrase: 'encender cuarta habitacion',
        turnOffPhrase: 'apagar cuarta habitacion',
        x: 272 * 2 + (272 / 2),
        y: 930
    }];

    provider = {
        lightBulbs: lightBulbs
    };


    if (!speech.ready) {
        speech.on('ready', function() {
            setup();
        });
    } else {
        setup();
    }


    return provider;
});
