define([
    'underscore'
], function(_) {

    var helper;

    helper = {

        buildTransitions: function(fromState, toState, words) {
            var transitions = [];

            _.each(words, function(word) {
                transitions.push({
                    from: fromState,
                    to: toState,
                    word: word
                });
            });

            return transitions;
        }

    };

    return helper;
});
