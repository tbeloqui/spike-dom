define([], function() {
    'use strict';

    var start = function(x, y, ev) {
        this.data('ot', this.transform().local);
    }

    var move = function(dx, dy, ev, x, y) {
        var tdx, tdy;
        var snapInvMatrix = this.transform().diffMatrix.invert();
        snapInvMatrix.e = snapInvMatrix.f = 0;
        tdx = snapInvMatrix.x(dx, dy);
        tdy = snapInvMatrix.y(dx, dy);
        this.transform("t" + [tdx, tdy] + this.data('ot'));
    }

    var stop = function() {}

    return {
        apply: function(component) {
            component.drag(move, start, stop);
        }
    }
});
