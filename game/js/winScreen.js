var WinScreenView = Backbone.View.extend({
    initialize: function(options) {

        this.r = options.r;
        this.s = Snap(1280, 800);
        this.s.attr({
            left: 0,
            top: 0,
            position: 'absolute',
            visibility: 'hidden'
        });

    },
    show: function() {

        var instance = this;
        this.s.attr({
            visibility: 'visible'
        });

        this.element = this.s.circle(
            $(window).width() >> 1, ($(window).height() >> 1) - 100,
            2);

        this.element.attr({
            fill: "#b7bf76",
            stroke: "#000",
            strokeWidth: 5
        });

        this.element.animate({
                r: 999
            }, 1 * 1000,
            function() {


                instance.s.text(($(window).width() >> 2) + 100, ($(window).height() >> 1) - 100, '¡Ganaste, Felicitaciones!').attr({
                    fill: '#FFF',
                    fontFamily: 'Comic Sans MS',
                    fontSize: '2em',
                    fontWeight: 'bold',
                    stroke: '#000',
                    strokeWidth: 1
                });

                setTimeout(function() {
                    window.location.href = '/';
                }, 5 * 1000);
            });
    }
});
