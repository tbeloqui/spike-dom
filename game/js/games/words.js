var speech = null;

var GameMode = {
    SPANISH: 'spanish',
    ENGLISH: 'english'
};

var Titles = [];
Titles.english = '¿Cómo se dice en español?';
Titles.spanish = '¿Cómo se dice en inglés?';

var words = [{
    english: 'hi',
    spanish: 'hola',
    speechWord: 'hola',
    img: {
        name: 'hi.jpg',
        width: 295,
        height: 248
    }
}, {
    english: 'how',
    spanish: 'como',
    speechWord: 'como',
    img: {
        name: 'how.png',
        width: 295,
        height: 248
    }
}, {
    english: 'you',
    spanish: 'usted',
    speechWord: 'usted',
    img: {
        name: 'you.jpg',
        width: 295,
        height: 248
    }
}, {
    english: 'you are',
    spanish: 'estas',
    speechWord: 'estas',
    img: {
        name: 'you_are.png',
        width: 295,
        height: 248
    }
}, {
    english: 'how are you?',
    spanish: '¿como estas?',
    speechWord: 'como estas',
    img: {
        name: 'how_are_you.jpg',
        width: 295,
        height: 248
    }
}, {
    english: 'I am',
    spanish: 'estoy',
    speechWord: 'estoy',
    img: {
        name: 'i_am.jpg',
        width: 295,
        height: 248
    }
}, {
    english: 'I am',
    spanish: 'soy',
    speechWord: 'soy',
    img: {
        name: 'i_am_2.jpg',
        width: 295,
        height: 248
    }
}, {
    english: 'very well',
    spanish: 'muy bien',
    speechWord: 'muy bien',
    img: {
        name: 'very_well.jpg',
        width: 295,
        height: 248
    }
}, {
    english: 'I am very well',
    spanish: 'estoy muy bien',
    speechWord: 'estoy muy bien',
    img: {
        name: 'i_am_very_well.jpg',
        width: 295,
        height: 248
    }
}, {
    english: 'thank you',
    spanish: 'gracias',
    speechWord: 'gracias',
    img: {
        name: 'thanks.png',
        width: 295,
        height: 248
    }
}, {
    english: 'please',
    spanish: 'por favor',
    speechWord: 'porfavor',
    img: {
        name: 'please.jpg',
        width: 295,
        height: 248
    }
}, {
    english: 'what',
    spanish: 'que',
    speechWord: 'que',
    img: {
        name: 'what.jpg',
        width: 295,
        height: 248
    }
}, {
    english: 'what is your name?',
    spanish: '¿cómo te llamas?',
    speechWord: 'como te llamas',
    img: {
        name: 'whats_your_name.jpg',
        width: 295,
        height: 248
    }
}];

/**
 *  * Shuffles array in place.
 *   * @param {Array} a items The array containing the items.
 *    */
function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i -= 1) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function createHelp(word, imgURLPRefix) {
    return {
        img: imgURLPRefix + word.img.name,
        x: word.img.x,
        y: word.img.y,
        width: word.img.width,
        height: word.img.height
    };
}

var WordsGame = Backbone.View.extend({

    initialize: function(options) {
        this.s = this.options.s;
        this.errorCount = 0;
        this.speechEnabled = false;

        speech = options.speech;

        var instance = this;

        var numbersSpeechWords = ['uno', 'dos', 'tres', 'cuatro'];
        _.each(numbersSpeechWords, function(speechWord) {
            speech.add(speechWord, instance.handleSpeech.bind(instance, speechWord, numbersSpeechWords.indexOf(speechWord)));
        });

        _.each(words, function(word) {
            speech.add(word.speechWord, instance.handleSpeech.bind(instance, word.speechWord, -1));
        });
    },

    reset: function() {},

    show: function() {

        var instance = this;
        var wordIndex = Math.ceil(Math.random() * words.length) - 1;
        var modeNumber = Math.ceil(Math.random() * 100);
        this.mode = modeNumber <= 50 ? GameMode.SPANISH : GameMode.ENGLISH;

        this.word = words[wordIndex];
        console.log('wordIndex', wordIndex, 'gameMode', this.mode, 'word', this.word);

        var documentWidth = $(document).width();
        var titleX = (documentWidth >> 2) + 20;
        var titleY = 50;

        this.title = this.s.text(titleX, titleY, Titles[this.mode]).attr({
            fill: '#FFF',
            fontFamily: 'Comic Sans MS',
            fontSize: '2em',
            fontWeight: 'bold',
            stroke: '#000',
            strokeWidth: 1
        });

        var wordString = this.mode == GameMode.ENGLISH ? this.word.english : this.word.spanish;
        var wordY = 150;
        var windowWidth = $(window).width();
        var wordX = windowWidth >> 1;

        if (wordString.length <= 2) {
            wordX -= 20;
        } else if (wordString.length >= 10) {
            wordX -= 260;
        } else /* if (wordString.length <= 4) */ {
            wordX -= 100;
        }

        console.log(wordString.length, wordX);

        this.wordText = this.s.text(wordX, wordY, wordString).attr({
            fill: '#FFF',
            fontFamily: 'Comic Sans MS',
            fontSize: '4em',
            fontWeight: 'bold',
            stroke: '#000',
            strokeWidth: 1
        });

        var maxAnswers = 3;
        this.wordOptionIds = [];

        do {
            var answerIndex = Math.ceil(Math.random() * words.length) - 1;
            // console.log('got answer index', answerIndex);
            if (this.wordOptionIds.indexOf(answerIndex) == -1 && answerIndex != wordIndex) {
                this.wordOptionIds.push(answerIndex);
            }

        } while (this.wordOptionIds.length < maxAnswers);

        this.wordOptionIds.push(wordIndex);
        shuffle(this.wordOptionIds);

        var wordOptionX = (windowWidth >> 1);
        if (this.mode == GameMode.ENGLISH) {
            wordOptionX -= 80;
        } else {
            wordOptionX -= 150;
        }

        var wordOptionY = wordY + 55;
        var wordOptionYDelta = 60;

        this.wordOptions = [];

        for (var i = 0; i < this.wordOptionIds.length; ++i) {
            wordOptionY += wordOptionYDelta;

            var wordOption = words[this.wordOptionIds[i]];
            var wordOptionString = this.mode == GameMode.ENGLISH ? wordOption.spanish : wordOption.english;
            this.wordOptions.push(this.createWordOption(this.mode, wordOption, wordOptionString, i + 1, wordOptionX, wordOptionY));
            console.log(wordOption, wordOptionString);
        }

        this.speechEnabled = true;
    },

    hide: function() {
        this.title.remove();
        this.wordText.remove();
        for (var i = 0; i < this.wordOptions.length; ++i) {
            this.wordOptions[i].remove();
        }
        this.speechEnabled = false;
    },

    createWordOption: function(mode, word, str, index, x, y) {

        var indexStr = '';

        if (mode == GameMode.SPANISH) {
            indexStr += '(Opción';
            switch (index) {
                case 1:
                    indexStr += ' uno)';
                    break;
                case 2:
                    indexStr += ' dos)';
                    break;
                case 3:
                    indexStr += ' tres)';
                    break;
                case 4:
                    indexStr += ' cuatro)';
                    break;
            }
        }

        var instance = this;
        var t = this.s.text(x, y, indexStr + ' ' + str);
        t.attr({
            cursor: 'pointer',
            'font-size': 25,
            fill: '#FFF',
            fontFamily: 'Comic Sans MS',
            fontWeight: 'bold',
            stroke: '#00c',
            strokeWidth: 1
        });

        t.click(function(ev) {
            console.log('clicked', word);
            instance.checkWordMatch(word, t);
        });
        return t;
    },

    checkWordMatch: function(word, textElement) {

        if (word == this.word) {
            console.log('onWordMatch');
            this.onWordMatch(textElement);
        } else {
            console.log('onWordNotMatch');
            this.onWordNotMatch(textElement);
        }
    },

    onWordMatch: function(textElement) {

        var instance = this;

        textElement.animate({
            fill: "#0d0"
        }, 500, function() {
            instance.hide();
            instance.trigger('win');
        });

    },

    onWordNotMatch: function(textElement) {
        var instance = this;

        textElement.animate({
            fill: "#d00"
        }, 500, function() {
            instance.handleError();
        });
    },

    handleError: function() {
        ++this.errorCount;
        if (this.errorCount >= 3) {
            this.errorCount = 0;
            this.speechEnabled = false;
            this.trigger('help', this.createHelp());
        }
    },

    createHelp: function() {
        var prefix = 'img/';
        return createHelp(this.word, prefix);
    },

    onHelpDeactivated: function() {
        this.speechEnabled = true;
    },

    handleSpeech: function(speechWord, speechWordIndex) {

        if (!this.speechEnabled) {
            return;
        }
        speech.stop();

        var wordIndex,
            optionIndex;

        if (this.mode == GameMode.SPANISH) {
            if (speechWordIndex >= 0) {
                optionIndex = speechWordIndex;
                wordIndex = this.wordOptionIds[speechWordIndex];
            }
        } else /* GameMode.ENGLISH */ {
            wordIndex = _.findIndex(words, function(word) {
                return word.english.toUpperCase() == speechWord.toUpperCase() ||
                    word.spanish.toUpperCase() == speechWord.toUpperCase() ||
                    word.speechWord.toUpperCase() == speechWord.toUpperCase();
            });
            optionIndex = this.wordOptionIds.indexOf(wordIndex);
        }

        console.log('words handleSpeech', 'speechWordIndex', speechWordIndex, 'speechWord', speechWord, 'wordIndex', wordIndex, 'optionIndex', optionIndex);

        if (wordIndex >= 0 && optionIndex >= 0) {
            this.checkWordMatch(words[wordIndex], this.wordOptions[optionIndex]);
        }
    }

});
