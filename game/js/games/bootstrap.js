var GameBootStrap = Backbone.View.extend({
    initialize: function() {
        this.s = Snap(1280, 800);
        this.s.attr({
            left: 0,
            top: 0,
            position: 'absolute',
            visibility: 'hidden'
        });
    },
    show: function() {
        var instance = this;
        this.s.attr({
            visibility: 'visible'
        });

        this.element = this.s.circle(
            $(window).width() >> 1, ($(window).height() >> 1) - 100,
            2);

        this.element.attr({
            fill: "#b7bf76",
            stroke: "#000",
            strokeWidth: 5
        });

        this.element.animate({
                r: 999
            }, 1 * 1000,
            function() {
                instance.trigger('ready');
            });

    },
    hide: function() {
        var instance = this;
        this.s.attr({
            visibility: 'visible'
        });

        this.element.animate({
                r: 0
            }, 1 * 1000,
            function() {
                instance.element.remove();
                instance.s.attr({
                    visibility: 'hidden'
                });
            });


    }
});
