var originalBubbleColor = "#d88254";
var originalBubbleRadio = 50;

function removeBubble(bubble) {
    bubble.element.animate({
            r: 0
        }, 1 * 100,
        function() {
            this.remove();
        }
    );

    bubble.numberText.remove();
}

function createHelpMinMax(bubble) {
    var x = ($(document).width() >> 3) + 70;
    var y = $(document).height() >> 2;
    var message = '';

    if (bubble.number > 0 || bubble.number === 10) {
        message += '\u2219 ' + (bubble.number - 1);
        if (bubble.number === 10) {
            x += 100;
        }
    }

    message += ' ' + bubble.number + ' ';

    if (bubble.number < 10 || bubble.number === 0) {
        message += (bubble.number + 1) + ' \u2219';
        if (bubble.number === 10) {
            x += 100;
        }
    }
    return {
        text: message,
        x: x,
        y: y,
        fontSize: '10em'
    };
}

var SpeechNumberMap = [{
    speechNumber: 'uno',
    number: 1
}, {
    speechNumber: 'dos',
    number: 2
}, {
    speechNumber: 'tres',
    number: 3
}, {
    speechNumber: 'cuatro',
    number: 4
}, {
    speechNumber: 'cinco',
    number: 5
}, {
    speechNumber: 'seis',
    number: 6
}, {
    speechNumber: 'siete',
    number: 7
}, {
    speechNumber: 'ocho',
    number: 8
}, {
    speechNumber: 'nueve',
    number: 9
}, {
    speechNumber: 'dies',
    number: 10
}];

var BubblesGame = Backbone.View.extend({
    initialize: function() {

        var instance = this;
        var speech = this.options.speech;
        this.speech = speech;

        _.each(SpeechNumberMap, function(item) {
            speech.add(item.speechNumber, instance.handleSpeech.bind(instance, item.number));
        });

        this.speechEnabled = false;

        this.s = this.options.s;
        // this.controller = this.options.controller;
        this.levels = [{
            title: 'Ordenar de menor a mayor',
            from: 1,
            to: 10,
            rule: function(bubble, bubbles) {

                for (var i = 0; i < bubbles.length; ++i) {

                    if (bubble.number > bubbles[i].number) {
                        return false;
                    }
                }

                return true;
            },
            createHelp: function(bubble) {
                return createHelpMinMax(bubble);
            }
        }, {
            title: 'Ordenar de mayor a menor',
            from: 1,
            to: 10,
            rule: function(bubble, bubbles) {
                for (var i = 0; i < bubbles.length; ++i) {

                    if (bubble.number < bubbles[i].number) {
                        return false;
                    }
                }

                return true;
            },
            createHelp: function(bubble) {
                return createHelpMinMax(bubble);
            }
        }, {
            title: 'Elegir multiplos de 2',
            from: 1,
            to: 10,
            rule: function(bubble, bubbles) {
                return (bubble.number % 2) === 0;
            },
            createHelp: function(bubble) {
                var x = ($(document).width() >> 3) + 70;
                var y = ($(document).height() >> 2) - 30;

                return {
                    text: "2 + 2 + 2 = 6, 6 es múltiplo de 2",
                    x: x,
                    y: y,
                    fontSize: '3em'
                };
            }
        }, {
            title: 'Elegir multiplos de 3',
            from: 1,
            to: 10,
            rule: function(bubble, bubbles) {
                return (bubble.number % 3) === 0;
            },
            createHelp: function(bubble) {
                var x = ($(document).width() >> 3) + 70;
                var y = ($(document).height() >> 2) - 30;

                return {
                    text: "3 + 3 + 3 = 9, 9 es múltiplo de 3",
                    x: x,
                    y: y,
                    fontSize: '3em'
                };
            }
        }];
        this.pickedBubbles = [];
        this.levelIndex = -1;
        this.level = null;
        this.errorCount = 0;
    },

    reset: function() {

    },

    show: function() {
        var i;
        this.bubbles = [];
        var tempBubbles = [];
        var bubble;
        var instance = this;

        var titleX = $(document).width() >> 2;
        var titleY = 50;

        var startingBubbleX = ($(document).width() >> 2) - (70);
        var bubbleX = startingBubbleX;
        var bubbleY = 150;
        var bubbleXOffset = 150;

        ++this.levelIndex;
        if (this.levelIndex >= this.levels.length) {
            this.levelIndex = 0;
        }
        // console.log('bubbles level', this.levelIndex);

        this.level = this.levels[this.levelIndex];

        this.level.titleText = this.s.text(titleX, titleY, this.level.title).attr({
            fill: '#FFF',
            fontFamily: 'Comic Sans MS',
            fontSize: '2em',
            fontWeight: 'bold',
            stroke: '#000',
            strokeWidth: 1
        });

        for (i = this.level.from; i <= this.level.to; ++i) {
            tempBubbles.push({
                number: i
            });
        }

        do {
            /* pick a random bubble */
            i = Math.ceil(Math.random() * tempBubbles.length) - 1;
            if (i >= 0 && i < tempBubbles.length) {

                /* change row */
                if (this.bubbles.length == 5) {
                    bubbleX = startingBubbleX;
                    bubbleY = 350;
                }

                /* get the bubble */
                bubble = tempBubbles[i];
                /* remove it from collection */
                tempBubbles.splice(i, 1);

                /* make bubble graphic */
                bubble.element = this.s.circle(bubbleX, bubbleY, originalBubbleRadio);
                // console.log('drawing bubble', bubbleX, bubbleY);

                bubble.element.attr({
                    fill: originalBubbleColor,
                    stroke: "#000",
                    strokeWidth: 5,
                    'cursor': 'pointer'
                });

                // console.log('drawing bubble number', bubble.number.toString());
                bubble.numberText = this.s.text(bubbleX - 10, bubbleY + 10, bubble.number.toString()).attr({
                    fill: '#000',
                    fontFamily: 'Comic Sans MS',
                    fontSize: '2em',
                    fontWeight: 'bold',
                    stroke: '#000',
                    strokeWidth: 0,
                    'cursor': 'pointer'
                });

                bubble.element.bubble = bubble;
                bubble.element.click(function() {
                    instance.pickBubble(this.bubble);
                });

                bubble.numberText.bubble = bubble;
                bubble.numberText.click(function() {
                    instance.pickBubble(this.bubble);
                });

                this.bubbles.push(bubble);
                bubbleX += bubbleXOffset;
            }

        } while (tempBubbles.length != 0);

        this.speechEnabled = true;
    },

    hide: function() {
        this.bubbles = [];
        this.level.titleText.remove();
        this.level = null;
        this.speechEnabled = true;
    },

    handleSpeech: function(number) {
        if (!this.speechEnabled) {
            return;
        }

        this.speech.stop();

        var bubbleIndex = _.findIndex(this.bubbles, function(item) {
            return item.number == number;
        });

        console.log('bubbles handleSpeech, bubble number:', number, ' bubbleIndex: ', bubbleIndex);

        if (bubbleIndex != -1) {
            var bubble = this.bubbles[bubbleIndex];
            this.pickBubble(bubble);
        }
    },

    pickBubble: function(bubble) {

        if (this.level.rule(bubble, this.bubbles)) {
            this.onBubblePicked(bubble);
        } else {
            this.onBubblePickedFail(bubble);
        }

    },

    onBubblePicked: function(bubble) {
        var bubbleIndex = this.bubbles.indexOf(bubble);
        if (bubbleIndex != -1) {
            // console.log('picked', bubble, bubbleIndex);

            removeBubble(bubble);

            this.bubbles.splice(bubbleIndex, 1);
            this.checkLevelWin();
        }
    },

    onBubblePickedFail: function(bubble) {
        // console.log('not picked', bubble);
        var instance = this;
        var originalR = originalBubbleRadio;

        /* animate radio */
        bubble.element.animate({
            r: originalR >> 1,
            fill: "#d00"
        }, 200, function() {

            /* animate to restore original conf */
            bubble.element.animate({
                r: originalR,
                fill: originalBubbleColor
            }, 200, function() {

                ++instance.errorCount;
                if (instance.errorCount >= 3) {
                    instance.errorCount = 0;
                    instance.speechEnabled = false;
                    instance.trigger('help', instance.level.createHelp(bubble));
                }
            });
        });

    },

    ruleWin: function(level, bubbles) {
        for (var i = 0; i < bubbles.length; ++i) {
            if (level.rule(bubbles[i], bubbles)) {
                return false;
            }
        }
        return true;
    },

    checkLevelWin: function() {
        if (this.ruleWin(this.level, this.bubbles)) {
            this.onGameWin();
        }
    },

    onGameWin: function() {
        this.hide();
        this.trigger('win');
    },

    onHelpDeactivated: function() {
        this.speechEnabled = true;
    }
});
