var speech = null;


var SpeechTextView = Backbone.View.extend({

    initialize: function(options) {


        var width = 300,
            height = 32,
            documentWidth = $(document).width();

        this.s = Snap(width, height);
        this.s.attr({
            top: 0,
            left: 300,
            position: 'absolute'
        });


        this.text = this.s.text(0, 20, '').attr({
            fontSize: '20px'
        });

        speech = options.speech;
        speech.on('result', this.handleSpeechResults.bind(this));
    },

    clear: function() {
        if (this.text) {
            this.text.attr({
                text: ''
            });
        }

    },

    handleSpeechResults: function(matches) {

        var str = '- ',
            splits = matches.split(" ");

        _.each(splits, function(match) {
            str += match.toUpperCase() + ' - ';
        });

        this.text.attr({
            text: str,
            fill: 'white'
        });

    }

});
