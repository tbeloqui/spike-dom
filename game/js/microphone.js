var speech = null;

function toggleRecording(mic) {

    if (mic.active) {
        speech.stop();
    } else {
        speech.start();
    }
}

function turnWhite(mic, el) {
    el.attr({
        fill: 'white',
        opacity: '0.75'
    });
    mic.active = false;
    // el.animate({
    //     fill: 'white',
    //     opacity: '0.75'
    // }, 0, mina.easeout, function() {
    //     mic.active = false;
    // });

}

function turnRed(mic, el) {
    el.attr({
        fill: '#FF0000',
        opacity: '0.75'
    });
    mic.active = true;
    // el.animate({
    //     fill: '#FF0000',
    //     opacity: '0.75'
    // }, 0, mina.easeout, function() {
    //     mic.active = true;
    // });
}

function wobble(mic, t, el) {

    if (t <= 0) {
        el.stop().animate({
            transform: 'r0 48,96'
        }, 100, mina.easeout, function() {
            turnRed(mic, el);
        });
        return;
    }
    var half = Math.round(t / 2);

    el.stop().animate({
        transform: 'r' + half + ' 48,96'
    }, 100 * half, mina.easeout, function() {
        el.stop().animate({
            transform: 'r-' + half + ' 48,96'
        }, 100 * half, mina.easeout, function() {
            --t;
            wobble(mic, t, el);
        });
    });

}


var MicrophoneView = Backbone.View.extend({
    initialize: function(options) {
        this.s = Snap(64, 93);
        this.s.attr({
            left: 124,
            top: 25,
            position: 'absolute',
        });

        this.active = false;
        this.ready = false;

        speech = options.speech;
        speech.on('started', this.showRecording.bind(this));
        speech.on('stopped', this.hideRecording.bind(this));

        if (speech.ready) {
            this.checkReady();
        } else {
            speech.on('ready', this.checkReady.bind(this));
        }

        Snap.load("svg/microphone.svg", this.onSVGLoaded.bind(this));
    },

    onSVGLoaded: function(f) {
        this.group = f.select('g');
        this.s.append(this.group);
        var m = new Snap.Matrix();
        m.translate(0, 0);
        this.group.attr({
            fill: 'white',
            opacity: 0.1,
            transform: m
        });

        var bb = this.group.getBBox();
        this.rect = this.s.rect(bb.x, bb.y, bb.width, bb.height);
        this.rect.attr({
            opacity: 0
        });
        this.rect.click(this.onClick.bind(this));

        this.mic = this.group.select('#mic');
        this.checkReady();
    },

    onClick: function() {
        if (this.ready) {
            toggleRecording(this);
        }
    },

    checkReady: function() {
        if (speech.ready) {
            this.ready = true;
            this.rect.attr({
                cursor: 'pointer'
            });
            this.show();
        }
    },

    show: function() {
        this.group.animate({
                opacity: '0.75',
            },
            1000,
            mina.elastic
        );
    },

    showRecording: function() {
        if (!this.active) {
            turnRed(this, this.mic);
        }

    },

    hideRecording: function() {
        if (this.active) {
            turnWhite(this, this.mic);
        }

    }
});
