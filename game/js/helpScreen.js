var HelpScreenView = Backbone.View.extend({
    initialize: function(o) {

        this.game = null;

        this.s = Snap(1280, 800);
        this.s.attr({
            left: 0,
            top: 0,
            position: 'absolute',
            visibility: 'hidden'
        });

        this.speech = o.speech;
        this.speech.add('gracias', this.handleSpeech.bind(this));

    },

    show: function(options) {
        var instance = this;
        this.s.attr({
            visibility: 'visible'
        });

        this.element = this.s.circle(
            $(window).width() >> 1, ($(window).height() >> 1) - 100,
            2);

        this.element.attr({
            fill: "#87cefa",
            stroke: "#000",
            strokeWidth: 5
        });

        this.element.animate({
                r: 999
            }, 1 * 1000,
            function() {

                instance.titleText = instance.s.text(($(window).width() >> 2) + 100, ($(window).height() >> 1) - 300, '¿Necesitas un poco de ayuda?').attr({
                    fill: '#FFF',
                    fontFamily: 'Comic Sans MS',
                    fontSize: '2em',
                    fontWeight: 'bold',
                    stroke: '#000',
                    strokeWidth: 1
                });

                if (options.text !== null && options.text !== undefined) {
                    instance.messageText = instance.s.text(options.x, options.y, options.text).attr({
                        fill: '#FFF',
                        fontFamily: 'Comic Sans MS',
                        fontSize: options.fontSize,
                        fontWeight: 'bold',
                        stroke: '#000',
                        strokeWidth: 1
                    });
                }

                if (options.img !== null && options.img !== undefined) {
                    var imgX = ($(window).width() >> 1) - (options.width >> 1);
                    var imgY = ($(window).height() >> 1) - options.height;
                    instance.img = instance.s.image(options.img, imgX, imgY, options.width, options.height);
                }

                var blockX = ($(document).width() >> 2) + 120,
                    blockY = ($(document).height() >> 2) + 150;
                instance.block = instance.s.rect(blockX, blockY, 75, 75, 20, 20);
                instance.block.attr({
                    fill: "rgb(236, 240, 241)",
                    stroke: "#1f2c39",
                    strokeWidth: 3,
                    cursor: 'pointer'
                });

                instance.blockText = instance.s.text(blockX + 25, blockY + 50, "¡Gracias!");
                instance.blockText.attr({
                    'font-size': 50,
                    cursor: 'pointer'
                });

                instance.block.attr({
                    width: (instance.blockText.node.clientWidth + 50)
                });

                instance.blockGroup = instance.s.g(instance.block, instance.blockText);
                instance.blockGroup.click(function(ev) {
                    instance.hide();
                });


                instance.visible = true;
            });
    },

    hide: function() {

        var instance = this;

        this.visible = false;
        this.titleText.remove();

        if (this.messageText !== null && this.messageText !== undefined) {
            this.messageText.remove();
        }

        if (this.img !== null && this.img !== undefined) {
            this.img.remove();
        }

        this.blockGroup.remove();
        this.block.remove();
        this.blockText.remove();

        this.element.animate({
            r: 0
        }, 300, function() {
            instance.element.remove();
            instance.s.attr({
                visibility: 'hidden'
            });

            if (instance.game !== null) {
                instance.game.onHelpDeactivated();
            }
        });

    },

    setGame: function(game) {
        this.game = game;
    },

    handleSpeech: function() {

        if (this.visible) {
            this.speech.stop();
            this.hide();
        }

    }
});
