var PathAnimal = Backbone.View.extend({
	initialize: function () {
		var _x,
			_y,
			PATHS = [
				'M0,0c0,0,0-5,0-10'
			];
		
		this.s = this.options.s;
		this.dot = this.options.dot;
		_x = this.dot.attr('cx');
		_y = this.dot.attr('cy');
		
		this.el = this.s.g();
		
		this.path = this.s.path(PATHS[Math.floor(Math.random() * PATHS.length)]);
		this.totalLength = this.dashOffset = this.path.getTotalLength();

		this.path.attr({
			fill: 'none',
			stroke: this.dot.attr('fill'),
			strokeWidth: 25,
			strokeMiterlimit: 10,
			strokeLinecap: 'round',
			opacity: 1,
			strokeDasharray: this.totalLength + " 200",
			strokeDashoffset: this.totalLength
		});
		this.el.add(this.path);
		
		this.el.transform("t" + [_x, _y]);
	},
	
	handle_MOUSEOVER: function (animateEndCallback) {
		var instance = this;
        
        Snap.animate(this.dashOffset, 0, function (val) {
            instance.dashOffset = val;
            instance.render();
        }, 500, animateEndCallback);
	},
	
	handle_MOUSEOUT: function () {
		var instance = this;
        
        Snap.animate(this.dashOffset, this.totalLength, function (val) {
            instance.dashOffset = val;
            instance.render();
        }, 500);
	},
	
	render: function () {
		var point;
		
		this.path.attr({
			'stroke-dashoffset': this.dashOffset
		});
		
		point = this.path.getPointAtLength(this.totalLength - this.dashOffset);
	}
});
