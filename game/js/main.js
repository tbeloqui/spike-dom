function buildTransitions(fromState, toState, words) {
    var transitions = [];

    _.each(words, function(word) {
        transitions.push({
            from: fromState,
            to: toState,
            word: word
        });
    });

    return transitions;
}


function setupSpeech(speech) {

    var words = [
        ['TIRAR', 'T I R A R'],
        ['DADO', 'D A D O'],
        ['UNO', 'U N O'],
        ['DOS', 'D O S'],
        ['TRES', 'T R E S'],
        ['CUATRO', 'K U A T R O'],
        ['CINCO', 'S I N K O'],
        ['SEIS', 'S E I S'],
        ['SIETE', 'S I E T E'],
        ['OCHO', 'O CH O'],
        ['nueve', 'N U U U E B E'],
        ['dies', 'D I E S S'],
        ['GRACIAS', 'G R A S I A S'],
        ['HOLA', 'O L A'],
        ['COMO', 'K O M O'],
        ['USTED', 'U S T E D'],
        ['ESTAS', 'E S T A S'],
        ['ESTOY', 'E S T O Y'],
        ['SOY', 'S O Y'],
        ['MUY', 'M U Y'],
        ['BIEN', 'B I E N'],
        ['PORFAVOR', 'P O R F A B O R'],
        ['QUE', 'K E'],
        ['TE', 'T E'],
        ['LLAMAS', 'Y A M A S']
    ];

    console.log('words', words);
    speech.addWords(words);


    var finalState = 8;
    var transitions = _.union(
        /* tirar dado */
        buildTransitions(0, 1, ['TIRAR']),
        buildTransitions(1, finalState, ['DADO']),

        /* UNO...DIES */
        buildTransitions(0, finalState, ['UNO']),
        buildTransitions(0, finalState, ['DOS']),
        buildTransitions(0, finalState, ['TRES']),
        buildTransitions(0, finalState, ['CUATRO']),
        buildTransitions(0, finalState, ['CINCO']),
        buildTransitions(0, finalState, ['SEIS']),
        buildTransitions(0, finalState, ['SIETE']),
        buildTransitions(0, finalState, ['OCHO']),
        buildTransitions(0, finalState, ['nueve']),
        buildTransitions(0, finalState, ['dies']),

        /* single words  */
        buildTransitions(0, finalState, ['GRACIAS']),
        buildTransitions(0, finalState, ['HOLA']),
        buildTransitions(0, finalState, ['COMO']),
        buildTransitions(0, finalState, ['USTED']),
        buildTransitions(0, finalState, ['ESTAS']),
        buildTransitions(0, finalState, ['ESTOY']),
        buildTransitions(0, finalState, ['SOY']),
        buildTransitions(0, finalState, ['PORFAVOR']),
        buildTransitions(0, finalState, ['QUE']),

        /* -como estas- */
        buildTransitions(0, 2, ['COMO']),
        buildTransitions(2, finalState, ['ESTAS']),

        /* -muy bien- */
        buildTransitions(0, 3, ['MUY']),
        buildTransitions(3, finalState, ['BIEN']),

        /* -estoy muy bien- */
        buildTransitions(0, 4, ['ESTOY']),
        buildTransitions(4, 5, ['MUY']),
        buildTransitions(5, finalState, ['BIEN']),

        /* -como te llamas- */
        buildTransitions(0, 6, ['COMO']),
        buildTransitions(6, 7, ['TE']),
        buildTransitions(7, finalState, ['LLAMAS'])
    );

    var grammar = {
        numStates: finalState + 1,
        start: 0,
        end: finalState,
        transitions: transitions
    };

    speech.addGrammar(grammar);
}

var MainView = Backbone.View.extend({
    initialize: function() {

        Backbone.history.start();

        var i,
            dotGroup,
            animal;

        Math.seedrandom('fish');

        var speech = new spike.SpeechModule(spike.SpeechModule.CMUSPHINX);
        if (!speech.ready) {
            speech.on('ready', function() {
                setupSpeech(speech);
            });
        } else {
            setupSpeech(speech);
        }

        this.speech = speech;
        this.rolling = false;
        this.speechEnabled = true;
        this.state = 0;
        this.dots = [];
        this.animals = [];
        this.trees = [];
        this.treeFaces = [];
        this.s = Snap(document.getElementsByTagName('svg')[0]);
        this.r = Backbone.Router.extend({});

        this.winScreen = new WinScreenView({
            r: this.r
        });

        this.gameBootstrap = new GameBootStrap();
        this.gameBootstrap.on('ready', this.startGame.bind(this));

        this.helpView = new HelpScreenView({
            speech: speech
        });

        this.games = [];
        this.games.push(new BubblesGame({
            s: this.gameBootstrap.s,
            speech: speech
        }));
        this.games.push(new WordsGame({
            s: this.gameBootstrap.s,
            speech: speech
        }));

        //dot animals
        dotGroup = this.s.select('#dots');
        this.dots = dotGroup.selectAll('*');

        for (i = 0; i < this.dots.length; i += 1) {
            animal = new PathAnimal({
                s: this.s,
                dot: this.dots[i]
            });
            this.animals.push(animal);
        }

        //sort depth
        for (i = 0; i < this.animals.length; i += 1) {
            if (i > 0) {
                var a = this.animals[i - 1].el,
                    b = this.animals[i].el;

                if (a.matrix.split().dy > b.matrix.split().dy) {
                    a.before(b);
                }
            }
        }

        //trees
        this.trees = this.s.selectAll('.tree');
        for (i = 0; i < this.trees.length; i += 1) {
            var tree = new TreeFace({
                s: this.s,
                tree: this.trees[i]
            });
            this.treeFaces.push(tree);
        }

        this.cube = document.getElementById('cube');
        var cubeHitArea = document.getElementById('cube-hitarea');
        cubeHitArea.addEventListener('click', this.handle_ROLL.bind(this));

        speech.add('tirar dado', this.handleSpeech.bind(this));
        speech.on('result', function(matches) {
            console.log('result', matches);
        });

        var microphone = new MicrophoneView({
            speech: speech
        });
        setTimeout(this.animate.bind(this), 3000);

        this.speechText = new SpeechTextView({
            speech: speech
        });

    },

    handleSpeech: function(matches) {
        if (!this.speechEnabled) {
            return;
        }
        this.speech.stop();
        this.handle_ROLL();
    },

    startGame: function() {

        if (!this.games || !this.games.length) {
            return;
        }

        this.speechText.clear();

        var instance = this;
        var gameIndex = Math.ceil(Math.random() * this.games.length) - 1;
        var game;

        console.log('got game index', gameIndex);

        if (gameIndex >= 0 && gameIndex < this.games.length) {
            game = this.games[gameIndex];
        }

        if (game) {
            game.reset(); /* resets level */
            game.show(); /* show the game */
            game.on('win', function() {

                setTimeout(function() {
                    instance.speechText.clear();
                    instance.gameBootstrap.hide();
                    instance.speechEnabled = true;
                    instance.rolling = false;

                    if (instance.state >= instance.animals.length) {
                        console.log('won game!');
                        instance.winScreen.show();
                    }

                }, 500);
            });
            game.on('help', function(options) {
                instance.helpView.show(options);
                instance.helpView.setGame(game);
            });
        } else {
            console.log('ERR no game found');
        }
    },

    handle_ROLL: function() {
        if (this.rolling) {
            return;
        }
        this.rolling = true;

        var value;
        do {
            value = Math.ceil(Math.random() * 6);
        } while (value == this.number);
        this.number = value;
        if (this.number == 6) {
            rx = 45;
            ry = 180;
            rz = -45;
        } else if (this.number == 5) {
            rx = 50;
            ry = 0;
            rz = 50;
        } else if (this.number == 4) {
            rx = -45;
            ry = 50;
            rz = 90;
        } else if (this.number == 3) {
            rx = -45;
            ry = 225;
            rz = -90;
        } else if (this.number == 2) {
            rx = -45;
            ry = 50;
            rz = 0;
        } else if (this.number == 1) {
            rx = 145;
            ry = -45;
            rz = 0;
        } else {
            rx = -90;
            ry = 0;
            rz = 0;
        }

        this.cube.style['webkitTransform'] = 'rotateX(' + rx + 'deg) rotateY(' + ry + 'deg) rotateZ(' + rz + 'deg)';
        this.cube.style['MozTransform'] = 'rotateX(' + rx + 'deg) rotateY(' + ry + 'deg) rotateZ(' + rz + 'deg)';
        this.trigger();
    },

    handleMouseOver: function() {

        var instance = this;
        instance.speechEnabled = false;
        setTimeout(function() {
            instance.gameBootstrap.show();
        }, 500);

    },

    trigger: function() {
        var instance = this;
        var prevState = this.state;
        this.state += this.number;
        if (this.state >= this.animals.length) {
            this.state = this.animals.length;
        }

        for (var i = prevState; i < this.state; ++i) {
            var animal = this.animals[i];

            if (i == (this.state - 1)) {
                /* set the callback for last animal path */
                animal.handle_MOUSEOVER(this.handleMouseOver.bind(this));
            } else {
                animal.handle_MOUSEOVER();
            }
        }
    },

    animate: function() {
        var tree = this.treeFaces[Math.floor(Math.random() * this.treeFaces.length)];
        tree.handle_MOUSEOVER();

        setTimeout(function() {
            tree.handle_MOUSEOUT();
        }.bind(this), 3000);

        setTimeout(this.animate.bind(this), 3000);
    }
});

var main = new MainView();
