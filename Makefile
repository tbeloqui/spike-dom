.PHONY: build serve

build:
	@mkdir -p build/lib
	@cp node_modules/jquery/dist/jquery.js build/lib
	@cp node_modules/requirejs/require.js build/lib
	@cp -fr src/* build/
	@cp -fr game build/
	@cp -fr assets build/
	@cp -fr css build/
	@cp -fr node_modules/bootstrap/dist/fonts build/
	@cp node_modules/bootstrap/dist/js/bootstrap.js build/lib/
	@cp node_modules/backbone/node_modules/underscore/underscore.js build/lib
	@cp node_modules/backbone/backbone.js build/lib
	@cp node_modules/backbone.localstorage/backbone.localStorage.js build/lib
	@cp node_modules/text/text.js build/lib
	@cp node_modules/snapsvg/dist/snap.svg.js build/lib

serve:
	@cd build && sudo node serve.js
