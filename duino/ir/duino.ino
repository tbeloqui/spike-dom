#include <IRremote.h>
#include <IRremoteInt.h>

#include "codes.h"

/* message format : !<code_index>.
 * <code_index> (3 digits) is the index of CODES, aka the selected code
*/
IRsend ir;
int index = 0;
int code_index = 0;

#define MESSAGE_LENGTH 3
char message[MESSAGE_LENGTH];

void setup()
{
  //Serial.begin(9600);
  Serial.begin(115200);
  Serial.println(F("arduino running"));
}

void loop()
{
  while(Serial.available() > 0) {
        char x = Serial.read();
        Serial.println(x);
        if (x == '!') {
            index = 0;
        } else if (x == '.') {
            process();
        } else {
            message[index++] = x;
            if (index >= MESSAGE_LENGTH) {
              index = 0;
            }
        }
   }
}

void process()
{
  code_index = atoi(message);
    if (code_index >= 0 && code_index < NUM_CODES) {
        sendIR();
    }
}

void sendIR()
{
#if defined(SEND_RAW)
ir.sendRaw(CODES[code_index], CODE_LENGTH, HZ);
#endif
}
