
/* message format: !nnn. */
#define MESSAGE_LENGTH 3
char message[MESSAGE_LENGTH];
int index = 0;

void setup()
{
  //Serial.begin(9600);
  Serial.begin(115200);
  Serial.println(F("arduino running"));
  
    pinMode(13, OUTPUT);
      pinMode(12, OUTPUT);
        pinMode(11, OUTPUT);
          pinMode(10, OUTPUT);
            pinMode(9, OUTPUT);
              pinMode(8, OUTPUT);
}

void loop()
{
  while(Serial.available() > 0) {
        char x = Serial.read();
        if (x == '!') {
            index = 0;
        } else if (x == '.') {
            process();
        } else {
            message[index++] = x;
            if (index >= MESSAGE_LENGTH) {
              index = 0;
            }
        }
   }
}

void process()
{
  char pinChar[3];
  pinChar[0] = message[0];
    pinChar[1] = message[1];
        pinChar[3] = '\0';
  int pin = atoi(pinChar);
  int enable = message[2] == '1';
  
  digitalWrite(pin, enable? HIGH : LOW);
    
}
